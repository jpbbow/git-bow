# Git - Bits of wisdom

## Split sub folder into own repository

**`man git subtree`**

Create a branch from the sub folder which you want to turn into an own
repository. Paths are relative to the repository root.

### Examples

**Important**: No leading or trailing characters in path for sub folder, e.g.
**NOT**

  - `path/to/subfolder/`
  - `./path/to/subfolder`
  - `./path/to/subfolder/`

#### General
Repository A: repository with the sub folder you want to branch off/convert into
own repository

Repository B: new repository for sub folder

  - In Repository A:
```
      git subtree split --prefix=path/to/subfolder/in/repo \
                        --branch=subfolder-branchname
```

 - Create Repository B:
  - `mkdir repoB`
  - `git init`
  - `git pull path/to/RepositoryA subfolder-branchname`

The man page example describe a push from the original to the new repository,
but it didn't work as smooth as the described pull.

## Orphan branches for specific publications

Create a branch from a repository which doesn't share history, e.g. create a
branch for a publication in a journal without sharing the whole history.

0. Create branch
    - `git checkout --orphan branch name`

0. Check if in branch
    - `git status`

0. Reset orphan branch
    - 'git reset'

0. Adjust new branch by removing files not wanted in current branch